terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.20"
    }
  }
}

provider "hcloud" {
  token = var.token
}

resource "hcloud_load_balancer" "lb" {
  name               = var.name
  load_balancer_type = var.type
  location           = var.location

  algorithm {
    type = var.algorithm
  }

  dynamic "target" {
    for_each = var.targets
    content {
      type           = "server"
      server_id      = target.value
      use_private_ip = true
    }
  }
}

resource "hcloud_load_balancer_network" "lb-net" {
  load_balancer_id = hcloud_load_balancer.lb.id
  subnet_id        = var.network
}

resource "hcloud_load_balancer_service" "lb-srv" {
  count            = length(var.services)
  load_balancer_id = hcloud_load_balancer.lb.id
  listen_port      = var.services[count.index]["listen_port"]
  destination_port = var.services[count.index]["destination_port"]
  protocol         = lookup(var.services[count.index], "protocol", "tcp")
  proxyprotocol    = lookup(var.services[count.index], "proxyprotocol", false)
}
