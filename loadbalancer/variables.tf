variable "token" {
  type        = string
  description = "HCloud API Token. https://console.hetzner.cloud/ -> Project -> Access -> API Tokens"
}

variable "location" {
  type    = string
  default = "hel1"
}

variable "type" {
  type    = string
  default = "lb11"
}

variable "name" {
  type    = string
  default = "load-balancer"
}

variable "network" {
  type = string
}

variable "algorithm" {
  type        = string
  default     = "round_robin"
  description = "Either round_robin or least_connections"
}

variable "targets" {
  type    = list
  default = []
}

variable "services" {
  type    = list
  default = []
}
