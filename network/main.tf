terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.20"
    }
  }
}

provider "hcloud" {
  token = var.token
}

resource "hcloud_network" "net" {
  name     = var.name
  ip_range = var.cidr
}

resource "hcloud_network_subnet" "net" {
  network_id   = hcloud_network.net.id
  type         = "server"
  ip_range     = hcloud_network.net.ip_range
  network_zone = "eu-central"
}
