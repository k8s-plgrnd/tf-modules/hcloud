# TODO: Descriptions
variable "token" {
  type        = string
  description = "HCloud API Token. https://console.hetzner.cloud/ -> Project -> Access -> API Tokens"
}

variable "name" {
  type    = string
  default = "hcloud-network"
}

variable "cidr" {
  type    = string
  default = "172.16.0.0/16"
}
