terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.20"
    }
  }
}

provider "hcloud" {
  token = var.token
}

resource "hcloud_ssh_key" "ssh_key" {
  for_each   = var.ssh_keys
  name       = each.key
  public_key = each.value
}
