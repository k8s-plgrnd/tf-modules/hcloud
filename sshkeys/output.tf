output "ids" {
  value = [for key in hcloud_ssh_key.ssh_key : key.id]
}
