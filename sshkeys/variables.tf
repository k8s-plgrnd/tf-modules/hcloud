variable "token" {
  type        = string
  description = "HCloud API Token. https://console.hetzner.cloud/ -> Project -> Access -> API Tokens"
}

variable "ssh_keys" {
  type    = map
  default = {}
}
