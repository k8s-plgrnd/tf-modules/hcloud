terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.20"
    }
  }
}

provider "hcloud" {
  token = var.token
}

data "hcloud_image" "image" {
  with_selector = var.image
  most_recent   = true
}

provider "random" {
  version = "~> 2.2"
}

resource "random_pet" "name" {
  count = var.amount
}

resource "hcloud_server" "server" {
  count       = var.amount
  name        = random_pet.name[count.index].id
  image       = data.hcloud_image.image.id
  server_type = var.type
  location    = var.locations[count.index % length(var.locations)]
  ssh_keys    = var.ssh_keys
}

resource "hcloud_server_network" "network" {
  count     = var.amount
  server_id = hcloud_server.server[count.index].id
  subnet_id = var.network
  ip        = cidrhost(var.ip_range, count.index + 1)
}
