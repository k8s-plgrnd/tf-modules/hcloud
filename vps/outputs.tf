locals {
  private_ips = { for node in hcloud_server_network.network : node.server_id => node.ip }
}

output "servers" {
  #value = hcloud_server.server
  value = [
    for i, server in concat(hcloud_server.server) : merge(server, { "private_ip" : local.private_ips[server.id] })
  ]
}
