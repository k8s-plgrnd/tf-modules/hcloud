# TODO: Descriptions

variable "token" {
  type        = string
  description = "HCloud API Token. https://console.hetzner.cloud/ -> Project -> Access -> API Tokens"
}

variable "amount" {
  type    = number
  default = 0
}

variable "locations" {
  type    = list
  default = ["fsn1", "nbg1", "hel1"]
}

variable "type" {
  type    = string
  default = "cx11"
}

variable "image" {
  type    = string
  default = "ubuntu-18.04"
}

variable "network" {
  type    = string
  default = 0
}

variable "ip_range" {
  type    = string
  default = ""
}

variable "ssh_keys" {
  type    = list(number)
  default = []
}
